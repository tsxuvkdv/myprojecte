/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "velo")
@XmlRootElement
@NamedQueries({
 @NamedQuery(name = "Velo.findAll", query = "SELECT v FROM Velo v"),
 @NamedQuery(name = "Velo.findById", query = "SELECT v FROM Velo v WHERE v.id = :id"),
 @NamedQuery(name = "Velo.findByTitle", query = "SELECT v FROM Velo v WHERE v.title = :title"),
 @NamedQuery(name = "Velo.findByPrice", query = "SELECT v FROM Velo v WHERE v.price = :price")})
public class Velo implements Serializable {

 private static final long serialVersionUID = 1L;
 @Id
 @Basic(optional = false)
 @NotNull
 @Column(name = "id")
 private Integer id;
 @Basic(optional = false)
 @NotNull
 @Size(min = 1, max = 128)
 @Column(name = "title")
 private String title;
 @Basic(optional = false)
 @NotNull
 @Lob
 @Size(min = 1, max = 65535)
 @Column(name = "text")
 private String text;
 @Basic(optional = false)
 @NotNull
 @Column(name = "price")
 private int price;

 public Velo() {
 }

 public Velo(Integer id) {
  this.id = id;
 }

 public Velo(Integer id, String title, String text, int price) {
  this.id = id;
  this.title = title;
  this.text = text;
  this.price = price;
 }

 public Integer getId() {
  return id;
 }

 public void setId(Integer id) {
  this.id = id;
 }

 public String getTitle() {
  return title;
 }

 public void setTitle(String title) {
  this.title = title;
 }

 public String getText() {
  return text;
 }

 public void setText(String text) {
  this.text = text;
 }

 public int getPrice() {
  return price;
 }

 public void setPrice(int price) {
  this.price = price;
 }

 @Override
 public int hashCode() {
  int hash = 0;
  hash += (id != null ? id.hashCode() : 0);
  return hash;
 }

 @Override
 public boolean equals(Object object) {
  // TODO: Warning - this method won't work in the case the id fields are not set
  if (!(object instanceof Velo)) {
   return false;
  }
  Velo other = (Velo) object;
  if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
   return false;
  }
  return true;
 }

 @Override
 public String toString() {
  return "entity.Velo[ id=" + id + " ]";
 }
 
}
