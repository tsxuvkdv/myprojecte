/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;
import entity.Velo;
import java.io.IOException;
import java.util.Enumeration;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import session.VeloFacade;

/**
 *
 * @author user
 */
@WebServlet(name = "NewServlet", loadOnStartup=1, urlPatterns = {"/admin.jsp","/velo.jsp","/zakaz.jsp"})
public class NewServlet extends HttpServlet {

 @EJB
 VeloFacade veloFacade;

 @Override
 public void init() throws ServletException {
  getServletContext().setAttribute("velo", veloFacade.findAll());
 }
 
 /**
  * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
  * methods.
  *
  * @param request servlet request
  * @param response servlet response
  * @throws ServletException if a servlet-specific error occurs
  * @throws IOException if an I/O error occurs
  */
 protected void processRequest(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
  
     response.setContentType("text/html;charset=UTF-8");
     String webPath=request.getServletPath();
     switch(webPath){
      case "/zakaz.jsp":
      case "/velo.jsp":
       String id=null;
       Enumeration<String> params = request.getParameterNames();
       while (params.hasMoreElements()){
        String param = params.nextElement();
        id="id".equals(param)?request.getParameter(param):id;
        try{
         Velo velo = veloFacade.find(Integer.parseInt(id));
         request.setAttribute("vel",velo);
        }catch(Exception e){   
        }
       }
       break;
      case "/admin.jsp":

       break;
     }
     request.getRequestDispatcher("/WEB-INF/view"+webPath).forward(request, response);        
  
 }

 // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
 /**
  * Handles the HTTP <code>GET</code> method.
  *
  * @param request servlet request
  * @param response servlet response
  * @throws ServletException if a servlet-specific error occurs
  * @throws IOException if an I/O error occurs
  */
 @Override
 protected void doGet(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
  processRequest(request, response);
 }

 /**
  * Handles the HTTP <code>POST</code> method.
  *
  * @param request servlet request
  * @param response servlet response
  * @throws ServletException if a servlet-specific error occurs
  * @throws IOException if an I/O error occurs
  */
 @Override
 protected void doPost(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
  processRequest(request, response);
 }

 /**
  * Returns a short description of the servlet.
  *
  * @return a String containing servlet description
  */
 @Override
 public String getServletInfo() {
  return "Short description";
 }// </editor-fold>

}
